package com.hybrid.codecli.util;

import com.hybrid.codecli.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserData {

    public static List<User> getUserData(){
        List<User> userData = new ArrayList<>();
        User user1 = new User();
        user1.setUserName("joe");
        user1.setId("1");
        user1.setEmailId("joe@gmail.com");

        User user2 = new User();
        user2.setUserName("tom");
        user2.setId("2");
        user2.setEmailId("tom@gmail.com");

        User user3 = new User();
        user3.setUserName("adam");
        user3.setId("3");
        user3.setEmailId("adam@gmail.com");
        userData.add(user1);
        userData.add(user2);
        userData.add(user3);
        return userData;
    }
}
