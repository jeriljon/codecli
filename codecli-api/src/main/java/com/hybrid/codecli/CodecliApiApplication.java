package com.hybrid.codecli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodecliApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodecliApiApplication.class, args);
	}

}
