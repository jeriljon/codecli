package com.hybrid.codecli.controller;

import com.hybrid.codecli.model.User;
import com.hybrid.codecli.util.UserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final Logger log = LoggerFactory.getLogger(UsersController.class);

    private List<User> userData = UserData.getUserData();

    @GetMapping("/")
    ResponseEntity<String> getWelcome() {
        log.debug("Get Welcome");
        String message = "Welcome to Code CLI";
        return  ResponseEntity.ok().body(message);
    }

    @GetMapping("/getUser/{id}")
    ResponseEntity<User> getUserById(@PathVariable Long id) {
        log.debug("Get User by id");
        User user = userData.get(id.intValue());
        return  ResponseEntity.ok().body(user);
    }

}
